using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class CategoriesController : ControllerBase
    {
        private readonly ISurveyCategoriesService _context;

        public CategoriesController(ISurveyCategoriesService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<SurveyCategory>> GetCategories()
        {
            return await _context.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetCategory")]
        public async Task<ActionResult<SurveyCategory>> GetCategory([FromRoute] string id)
        {
            var category = await _context.Get(id);
            if (category == null)
            {
                return NotFound();
            }

            return Ok(category);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] SurveyCategory category)
        {
            await _context.Create(category);
            return CreatedAtRoute("GetCategory", new { id = category.Id }, category);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<IActionResult> Update([FromRoute] string id, [FromBody] SurveyCategory categoryIn)
        {
            if (categoryIn.Id != id)
            {
                return BadRequest();
            }

            var category = await _context.Get(id);
            if (category == null)
            {
                return NotFound();
            }

            await _context.Update(id, categoryIn);
            return Ok(categoryIn);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> Delete([FromRoute] string id)
        {
            var category = await _context.Get(id);

            if (category == null)
            {
                return NotFound();
            }

            await _context.Remove(id);
            return Ok();
        }
    }
}