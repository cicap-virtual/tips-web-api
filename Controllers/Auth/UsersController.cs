using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {

        private readonly IUsersService _context;

        public UsersController(IUsersService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<User>> Get()
        {
            return await _context.Get();
        }

        [HttpGet("admin")]
        public async Task<IEnumerable<User>> GetAdmins()
        {
            return await _context.GetAdmins();
        }

        [HttpGet("company_admins")]
        public async Task<IEnumerable<User>> GetCompanyAdmins()
        {
            return await _context.GetCompanyAdmins();
        }

        [HttpGet("byemail/{email}")]
        public async Task<ActionResult<User>> GetByEmail([FromRoute] string email)
        {
            var user = await _context.GetByEmail(email);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);

        }

        [HttpGet("{id:length(24)}", Name = "GetUser")]
        public async Task<IActionResult> Get([FromRoute] string id)
        {
            var user = await _context.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] User user)
        {
            if (await _context.EmailAvailable(user.Email))
            {
                await _context.Create(user);
                return CreatedAtRoute("GetUser", new { id = user.Id }, user);
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPut("{id:length(24)}")]
        public async Task<ActionResult> Update([FromRoute] string id, [FromBody] User userIn)
        {
            if (userIn.Id != id)
            {
                return BadRequest();
            }

            var user = _context.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            await _context.Update(id, userIn);

            return Ok(userIn);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> Delete([FromRoute] string id)
        {
            var user = await _context.Get(id);

            if (user == null)
            {
                return NotFound();
            }

            await _context.Remove(user.Id);
            return Ok();
        }
    }

    public class PasswordObject
    {
        public string Id { get; set; }
        public string Password { get; set; }
    }
}