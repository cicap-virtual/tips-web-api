using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class RolesController : ControllerBase
    {
        private readonly IRoleService _context;

        public RolesController(IRoleService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<Role>> Get()
        {
            return await _context.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetRole")]
        public async Task<IActionResult> Get([FromRoute] string id)
        {
            var role = await _context.Get(id);

            if (role == null)
            {
                return NotFound();
            }

            return Ok(role);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] Role role)
        {
            await _context.Create(role);
            return CreatedAtRoute("GetRole", new { id = role.Id }, role);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<ActionResult> Update([FromRoute] string id, [FromBody] Role roleIn)
        {
            if (roleIn.Id != id)
            {
                return BadRequest();
            }

            var role = _context.Get(id);

            if (role == null)
            {
                return NotFound();
            }

            await _context.Update(id, roleIn);

            return Ok(roleIn);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> Delete([FromRoute] string id)
        {
            var role = await _context.Get(id);

            if (role == null)
            {
                return NotFound();
            }

            await _context.Remove(role.Id);
            return Ok();
        }
    }
}