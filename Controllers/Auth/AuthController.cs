using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Filters;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IUsersService _context;

        public AuthController(IUsersService context)
        {
            _context = context;
        }

        [SkipActionFilter]
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] FireBaseLog log)
        {
            var user = await _context.FireBaseLogin(log);

            if (user == null)
            {
                return NotFound();
            }

            var session = await _context.CreateSession(user);

            return Ok(session);
        }

        [HttpPost("logout")]
        public async Task<IActionResult> Login([FromBody] Session session)
        {
            await _context.DestroySession(session.Id);
            return NoContent();
        }

    }
}
