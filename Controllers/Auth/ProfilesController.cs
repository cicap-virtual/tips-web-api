using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProfilesController : ControllerBase
    {

        private readonly IProfilesService _context;

        public ProfilesController(IProfilesService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<Profile>> Get()
        {
            return await _context.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetProfile")]
        public async Task<ActionResult<Profile>> GetProfile([FromRoute] string id)
        {
            var profile = await _context.Get(id);

            if (profile == null)
            {
                return NotFound();
            }

            return Ok(profile);
        }

        [HttpGet("multiple")]
        [Produces("application/json", Type = typeof(List<Profile>))]
        public async Task<IActionResult> GetProfile([FromBody] List<string> ids)
        {
            var profiles = await _context.GetProfiles(ids);
            return Ok(profiles);
        }

        [HttpPost]
        public async Task<IActionResult> CreateProfile([FromBody] Profile profile)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (await _context.EmailAvailable(profile.Email))
            {
                await _context.Create(profile);

                return CreatedAtRoute("GetProfile", new { id = profile.Id }, profile);
            }
            else
            {
                return Unauthorized();
            }

        }

        [HttpGet("count")]
        public async Task<IActionResult> GetCount()
        {
            var count = await _context.GetCount();
            return Ok(count);
        }

        [HttpGet("{id:length(24)}/user")]
        [Produces("application/json", Type = typeof(Profile))]
        public async Task<IActionResult> GetUserProfile([FromRoute] string id)
        {
            var profile = await _context.GetUserProfile(id);
            if (profile == null)
            {
                return NotFound();
            }

            return Ok(profile);
        }

        [HttpGet("{id:length(24)}/pendings")]
        public async Task<IEnumerable<Schedule>> GetPendings([FromRoute] string id)
        {
            return await _context.GetPendings(id);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<ActionResult> Update([FromRoute] string id, [FromBody] Profile profileIn)
        {
            if (profileIn.Id != id)
            {
                return BadRequest();
            }

            var profile = _context.Get(id);

            if (profile == null)
            {
                return NotFound();
            }

            await _context.Update(id, profileIn);

            return Ok(profileIn);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> Delete([FromRoute] string id)
        {
            var profile = await _context.Get(id);

            if (profile == null)
            {
                return NotFound();
            }

            await _context.Remove(profile.Id);
            return Ok();
        }
    }
}