using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class PermissionsController : ControllerBase
    {
        private readonly IPermissionService _context;

        public PermissionsController(IPermissionService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<Permission>> Get()
        {
            return await _context.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetPermission")]
        public async Task<IActionResult> Get([FromRoute] string id)
        {
            var permission = await _context.Get(id);

            if (permission == null)
            {
                return NotFound();
            }

            return Ok(permission);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] Permission permission)
        {
            await _context.Create(permission);
            return CreatedAtRoute("GetPermission", new { id = permission.Id }, permission);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<ActionResult> Update([FromRoute] string id, [FromBody] Permission permissionIn)
        {
            if (permissionIn.Id != id)
            {
                return BadRequest();
            }

            var permission = _context.Get(id);

            if (permission == null)
            {
                return NotFound();
            }

            await _context.Update(id, permissionIn);

            return Ok(permissionIn);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> Delete([FromRoute] string id)
        {
            var permission = await _context.Get(id);

            if (permission == null)
            {
                return NotFound();
            }

            await _context.Remove(permission.Id);
            return Ok();
        }
    }
}