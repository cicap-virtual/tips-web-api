using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class MarketController : ControllerBase
    {
        private readonly IMarketService _context;

        public MarketController(IMarketService context)
        {
            _context = context;
        }

        [HttpGet("surveys")]
        public async Task<IEnumerable<Survey>> GetSurveys()
        {
            return await _context.GetSurveys();
        }

    }
}