using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class DivisionsController : ControllerBase
    {
        private readonly IDivisionsService _context;

        public DivisionsController(IDivisionsService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<Division>> Get()
        {
            return await _context.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetDivision")]
        public async Task<ActionResult<Division>> Get([FromRoute] string id)
        {
            var division = await _context.Get(id);

            if (division == null)
            {
                return NotFound();
            }

            return Ok(division);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] Division division)
        {
            await _context.Create(division);
            return CreatedAtRoute("GetDivision", new { id = division.Id }, division);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<ActionResult> Update([FromRoute] string id, [FromBody] Division divisionIn)
        {
            if (divisionIn.Id != id)
            {
                return BadRequest();
            }

            var division = _context.Get(id);

            if (division == null)
            {
                return NotFound();
            }

            await _context.Update(id, divisionIn);

            return Ok(divisionIn);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> Delete([FromRoute] string id)
        {
            var division = await _context.Get(id);

            if (division == null)
            {
                return NotFound();
            }

            await _context.Remove(division.Id);
            return Ok();
        }
    }
}