using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class CompaniesController : ControllerBase
    {
        private readonly ICompaniesService _context;

        public CompaniesController(ICompaniesService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<CompanyInfo>> Get()
        {
            return await _context.Get();
        }

        [HttpGet("{id:length(24)}/profiles")]
        public async Task<IEnumerable<Profile>> GetProfiles([FromRoute] string id)
        {
            return await _context.GetProfiles(id);
        }

        [HttpGet("{id:length(24)}/administrators")]
        public async Task<IEnumerable<User>> GetAdministrators([FromRoute] string id)
        {
            return await _context.GetAdministrators(id);
        }

        [HttpGet("{id:length(24)}", Name = "GetCompany")]
        public async Task<ActionResult<Company>> Get([FromRoute] string id)
        {
            var company = await _context.Get(id);

            if (company == null)
            {
                return NotFound();
            }

            return Ok(company);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] Company company)
        {
            await _context.Create(company);
            return CreatedAtRoute("GetCompany", new { id = company.Id }, company);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<ActionResult> Update([FromRoute] string id, [FromBody] Company companyIn)
        {
            if (companyIn.Id != id)
            {
                return BadRequest();
            }

            var company = _context.Get(id);

            if (company == null)
            {
                return NotFound();
            }

            await _context.Update(id, companyIn);

            return Ok(companyIn);
        }

        [HttpGet("{id:length(24)}/hierarchy")]
        public async Task<IEnumerable<Hierarchy>> GetHierarchies([FromRoute] string id)
        {
            return await _context.GetHierarchies(id);
        }

        [HttpGet("{id:length(24)}/parents")]
        public async Task<IEnumerable<Hierarchy>> GetParents([FromRoute] string id)
        {
            return await _context.GetParentsHierarchy(id);
        }

        [HttpGet("{id:length(24)}/schedules")]
        public async Task<IEnumerable<Schedule>> GetSchedules([FromRoute] string id)
        {
            return await _context.GetSchedules(id);
        }

        [HttpGet("{id:length(24)}/organization")]
        public async Task<IEnumerable<OrgHierarchy>> GetOrganizations([FromRoute] string id)
        {
            return await _context.GetOrgHierarchies(id);
        }

        [HttpGet("{id:length(24)}/positions")]
        public async Task<IEnumerable<Hierarchy>> GetPositions([FromRoute] string id)
        {
            return await _context.GetPositions(id);
        }


        [HttpPost("profiles/{id:length(24)}")]
        public async Task<ActionResult<Profile>> CreateProfiles([FromRoute] string id, [FromBody] ProfileItem profile)
        {
            var item = await _context.AddProfile(id, profile);

            return Ok(item);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> Delete([FromRoute] string id)
        {
            var company = await _context.Get(id);

            if (company == null)
            {
                return NotFound();
            }

            await _context.Remove(company.Id);
            return Ok();
        }
    }
}