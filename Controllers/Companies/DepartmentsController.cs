using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DepartmentsController : ControllerBase
    {
        private readonly IDepartmentsService _context;

        public DepartmentsController(IDepartmentsService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<Department>> Get()
        {
            return await _context.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetDepartment")]
        public async Task<ActionResult<Department>> Get([FromRoute] string id)
        {
            var department = await _context.Get(id);

            if (department == null)
            {
                return NotFound();
            }

            return Ok(department);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] Department department)
        {
            await _context.Create(department);
            return CreatedAtRoute("GetDepartment", new { id = department.Id }, department);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<ActionResult> Update([FromRoute] string id, [FromBody] Department departmentIn)
        {
            if (departmentIn.Id != id)
            {
                return BadRequest();
            }

            var department = _context.Get(id);

            if (department == null)
            {
                return NotFound();
            }

            await _context.Update(id, departmentIn);

            return Ok(departmentIn);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> Delete([FromRoute] string id)
        {
            var department = await _context.Get(id);

            if (department == null)
            {
                return NotFound();
            }

            await _context.Remove(department.Id);
            return Ok();
        }
    }
}