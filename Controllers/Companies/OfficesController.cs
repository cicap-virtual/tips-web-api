using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class OfficesController : ControllerBase
    {
        private readonly IOfficeService _context;

        public OfficesController(IOfficeService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<Office>> Get()
        {
            return await _context.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetOffice")]
        public async Task<ActionResult<Office>> Get([FromRoute] string id)
        {
            var office = await _context.Get(id);

            if (office == null)
            {
                return NotFound();
            }

            return Ok(office);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] Office office)
        {
            await _context.Create(office);
            return CreatedAtRoute("GetOffice", new { id = office.Id }, office);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<ActionResult> Update([FromRoute] string id, [FromBody] Office officeIn)
        {
            if (officeIn.Id != id)
            {
                return BadRequest();
            }

            var office = _context.Get(id);

            if (office == null)
            {
                return NotFound();
            }

            await _context.Update(id, officeIn);

            return Ok(officeIn);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> Delete([FromRoute] string id)
        {
            var office = await _context.Get(id);

            if (office == null)
            {
                return NotFound();
            }

            await _context.Remove(office.Id);
            return Ok();
        }
    }
}