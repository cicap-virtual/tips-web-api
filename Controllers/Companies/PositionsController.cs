using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class PositionsController : ControllerBase
    {
        private readonly IPositionsService _context;

        public PositionsController(IPositionsService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<Position>> Get()
        {
            return await _context.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetPosition")]
        public async Task<ActionResult<Position>> Get([FromRoute] string id)
        {
            var position = await _context.Get(id);

            if (position == null)
            {
                return NotFound();
            }

            return Ok(position);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] Position position)
        {
            await _context.Create(position);
            return CreatedAtRoute("GetPosition", new { id = position.Id }, position);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<ActionResult> Update([FromRoute] string id, [FromBody] Position positionIn)
        {
            if (positionIn.Id != id)
            {
                return BadRequest();
            }

            var position = _context.Get(id);

            if (position == null)
            {
                return NotFound();
            }

            await _context.Update(id, positionIn);

            return Ok(positionIn);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> Delete([FromRoute] string id)
        {
            var position = await _context.Get(id);

            if (position == null)
            {
                return NotFound();
            }

            await _context.Remove(position.Id);
            return Ok();
        }
    }
}