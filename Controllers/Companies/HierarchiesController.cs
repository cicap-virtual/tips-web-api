using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HierarchiesController : ControllerBase
    {
        private readonly IHierarchiesService _context;

        public HierarchiesController(IHierarchiesService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<Hierarchy>> GetAll()
        {
            return await _context.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetHierarchy")]
        public async Task<ActionResult<Hierarchy>> Get([FromRoute] string id)
        {
            var hierarchy = await _context.Get(id);

            if (hierarchy == null)
            {
                return NotFound();
            }

            return Ok(hierarchy);
        }

        [HttpGet("{id:length(24)}/employees")]
        public async Task<IEnumerable<Profile>> GetEmployees([FromRoute] string id)
        {
            return await _context.GetEmployees(id);
        }

        [HttpPost("employees")]
        public async Task<IEnumerable<Profile>> GetProfiles([FromBody] List<string> ids)
        {
            return await _context.GetMultiplesEmployees(ids);
        }

        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Hierarchy hierarchy)
        {
            await _context.Create(hierarchy);
            return CreatedAtRoute("GetHierarchy", new { id = hierarchy.Id }, hierarchy);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<IActionResult> Update([FromRoute] string id, [FromBody] Hierarchy hierarchyIn)
        {
            if (hierarchyIn.Id != id)
            {
                return BadRequest();
            }

            var hierarchy = _context.Get(id);

            if (hierarchy == null)
            {
                return NotFound();
            }

            await _context.Update(id, hierarchyIn);
            return Ok(hierarchyIn);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> Delete([FromRoute] string id)
        {
            var hierarchy = await _context.Get(id);

            if (hierarchy == null)
            {
                return NotFound();
            }

            await _context.Remove(hierarchy.Id);

            return Ok();
        }
    }
}