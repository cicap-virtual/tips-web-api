using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AnswerSetsController : ControllerBase
    {
        private readonly IAnswerSetService _context;

        public AnswerSetsController(IAnswerSetService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<AnswerSet>> Get()
        {
            return await _context.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetAnswerSet")]
        public async Task<IActionResult> Get([FromRoute] string id)
        {
            var set = await _context.Get(id);

            if (set == null)
            {
                return NotFound();
            }

            return Ok(set);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] AnswerSet set)
        {
            await _context.Create(set);
            return CreatedAtRoute("GetAnswerSet", new { id = set.Id }, set);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<ActionResult> Update([FromRoute] string id, [FromBody] AnswerSet setIn)
        {
            if (setIn.Id != id)
            {
                return BadRequest();
            }

            var set = _context.Get(id);

            if (set == null)
            {
                return NotFound();
            }

            await _context.Update(id, setIn);

            return Ok(setIn);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> Delete([FromRoute] string id)
        {
            var set = await _context.Get(id);

            if (set == null)
            {
                return NotFound();
            }

            await _context.Remove(set.Id);
            return Ok();
        }
    }
}