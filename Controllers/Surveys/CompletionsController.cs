using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class CompletionsController : ControllerBase
    {

        private readonly ICompletionsService _context;

        public CompletionsController(ICompletionsService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<Completion>> Get()
        {
            return await _context.Get();
        }

        [HttpGet("{id:length(24)}/results")]
        public async Task<ActionResult<Result>> GetResults([FromRoute] string id)
        {
            var result = await _context.GetResult(id);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet("{id:length(24)}", Name = "GetCompletion")]
        public async Task<IActionResult> GetCompletion([FromRoute] string id)
        {
            var completion = await _context.Get(id);

            if (completion == null)
            {
                return NotFound();
            }

            return Ok(completion);
        }

        [HttpPost]
        public async Task<IActionResult> CreateCompletion([FromBody] Completion completion)
        {
            await _context.Create(completion);
            return CreatedAtRoute("GetCompletion", new { id = completion.Id }, completion);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> Delete([FromRoute] string id)
        {
            var completion = await _context.Get(id);

            if (completion == null)
            {
                return NotFound();
            }

            await _context.Remove(completion.Id);
            return Ok();
        }

    }
}