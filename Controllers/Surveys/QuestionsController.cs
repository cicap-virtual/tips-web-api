using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class QuestionsController : ControllerBase
    {

        private readonly IQuestionsService _context;

        public QuestionsController(IQuestionsService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<Question>> Get()
        {
            return await _context.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetQuestion")]
        public async Task<IActionResult> Get([FromRoute] string id)
        {
            var question = await _context.Get(id);

            if (question == null)
            {
                return NotFound();
            }

            return Ok(question);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] Question question)
        {
            await _context.Create(question);
            return CreatedAtRoute("GetQuestion", new { id = question.Id }, question);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<ActionResult> Update([FromRoute] string id, [FromBody] Question questionIn)
        {
            if (questionIn.Id != id)
            {
                return BadRequest();
            }

            var question = _context.Get(id);

            if (question == null)
            {
                return NotFound();
            }

            await _context.Update(id, questionIn);

            return Ok(questionIn);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> Delete([FromRoute] string id)
        {
            var question = await _context.Get(id);

            if (question == null)
            {
                return NotFound();
            }

            await _context.Remove(question.Id);
            return Ok();
        }
    }
}