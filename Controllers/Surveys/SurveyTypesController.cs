using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class SurveyTypesController : ControllerBase
    {

        private readonly ISurveyTypeService _context;

        public SurveyTypesController(ISurveyTypeService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<SurveyType>> Get()
        {
            return await _context.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetType")]
        public async Task<IActionResult> Get([FromRoute] string id)
        {
            var type = await _context.Get(id);

            if (type == null)
            {
                return NotFound();
            }

            return Ok(type);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] SurveyType type)
        {
            await _context.Create(type);
            return CreatedAtRoute("GetType", new { id = type.Id }, type);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<ActionResult> Update([FromRoute] string id, [FromBody] SurveyType typeIn)
        {
            if (typeIn.Id != id)
            {
                return BadRequest();
            }

            var type = _context.Get(id);

            if (type == null)
            {
                return NotFound();
            }

            await _context.Update(id, typeIn);

            return Ok(typeIn);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> Delete([FromRoute] string id)
        {
            var type = await _context.Get(id);

            if (type == null)
            {
                return NotFound();
            }

            await _context.Remove(type.Id);
            return Ok();
        }
    }
}