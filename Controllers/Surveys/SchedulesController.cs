using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SchedulesController : ControllerBase
    {

        private readonly ISchedulesService _context;

        public SchedulesController(ISchedulesService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<Schedule>> Get()
        {
            return await _context.Get();
        }

        [HttpGet("{id:length(24)}/survey")]
        public async Task<IActionResult> GetSurvey([FromRoute] string id)
        {
            var survey = await _context.GetSurvey(id);

            if (survey == null)
            {
                return NotFound();
            }

            return Ok(survey);
        }

        [HttpGet("count")]
        public async Task<IActionResult> GetCount()
        {
            var count = await _context.GetCount();
            return Ok(count);
        }

        [HttpGet("{id:length(24)}/completions")]
        public async Task<IEnumerable<Completion>> GetCompletions([FromRoute] string id)
        {
            return await _context.GetCompletions(id);
        }

        [HttpGet("{id:length(24)}/results")]
        public async Task<IEnumerable<Result>> GetResults([FromRoute] string id) {
            return await _context.GetResults(id);
        }

        [HttpGet("{id:length(24)}", Name = "GetSchedule")]
        public async Task<IActionResult> Get([FromRoute] string id)
        {
            var schedule = await _context.Get(id);

            if (schedule == null)
            {
                return NotFound();
            }

            return Ok(schedule);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] Schedule schedule)
        {
            await _context.Create(schedule);
            return CreatedAtRoute("GetSchedule", new { id = schedule.Id }, schedule);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<ActionResult> Update([FromRoute] string id, [FromBody] Schedule scheduleIn)
        {
            if (scheduleIn.Id != id)
            {
                return BadRequest();
            }

            var schedule = _context.Get(id);

            if (schedule == null)
            {
                return NotFound();
            }

            await _context.Update(id, scheduleIn);

            return Ok(scheduleIn);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> Delete([FromRoute] string id)
        {
            var schedule = await _context.Get(id);

            if (schedule == null)
            {
                return NotFound();
            }

            await _context.Remove(schedule.Id);
            return Ok();
        }
    }
}