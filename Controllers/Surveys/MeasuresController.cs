using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MeasuresController : ControllerBase
    {
        private readonly IMeasuresService _context;

        public MeasuresController(IMeasuresService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<Measure>> Get()
        {
            return await _context.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetMeasure")]
        public async Task<IActionResult> Get([FromRoute] string id)
        {
            var measure = await _context.Get(id);

            if (measure == null)
            {
                return NotFound();
            }

            return Ok(measure);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] Measure measure)
        {
            await _context.Create(measure);
            return CreatedAtRoute("GetMeasure", new { id = measure.Id }, measure);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<ActionResult> Update([FromRoute] string id, [FromBody] Measure measureIn)
        {
            if (measureIn.Id != id)
            {
                return BadRequest();
            }

            var measure = _context.Get(id);

            if (measure == null)
            {
                return NotFound();
            }

            await _context.Update(id, measureIn);

            return Ok(measureIn);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> Delete([FromRoute] string id)
        {
            var measure = await _context.Get(id);

            if (measure == null)
            {
                return NotFound();
            }

            await _context.Remove(measure.Id);
            return Ok();
        }
    }
}