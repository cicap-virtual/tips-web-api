using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class SurveysController : ControllerBase
    {

        private readonly ISurveysService _context;

        public SurveysController(ISurveysService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<Survey>> Get()
        {
            return await _context.Get();
        }

        [HttpGet("count")]
        public async Task<IActionResult> GetCount()
        {
            var count = await _context.GetCount();

            return Ok(count);
        }

        [HttpGet("{id:length(24)}/temp")]
        public async Task<IEnumerable<Survey>> GetTemp([FromRoute] string id)
        {
            return await _context.GetTemp(id);
        }

        [HttpGet("{id:length(24)}", Name = "GetSurvey")]
        public async Task<IActionResult> Get([FromRoute] string id)
        {
            var survey = await _context.Get(id);

            if (survey == null)
            {
                return NotFound();
            }

            return Ok(survey);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] Survey survey)
        {
            await _context.Create(survey);
            return CreatedAtRoute("GetSurvey", new { id = survey.Id }, survey);
        }

        [HttpPost("temp")]
        public async Task<IActionResult> SaveTemp([FromBody] Survey survey)
        {
            await _context.SaveTemp(survey);
            return Ok(survey);
        }


        [HttpPut("{id:length(24)}")]
        public async Task<ActionResult> Update([FromRoute] string id, [FromBody] Survey surveyIn)
        {
            if (surveyIn.Id != id)
            {
                return BadRequest();
            }

            var survey = _context.Get(id);

            if (survey == null)
            {
                return NotFound();
            }

            await _context.Update(id, surveyIn);

            return Ok(surveyIn);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> Delete([FromRoute] string id)
        {
            var survey = await _context.Get(id);

            if (survey == null)
            {
                return NotFound();
            }

            await _context.Remove(survey.Id);
            return Ok();
        }
    }
}