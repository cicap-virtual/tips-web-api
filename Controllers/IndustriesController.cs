using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class IndustriesController : ControllerBase
    {
        private readonly IIndustriesService _context;

        public IndustriesController(IIndustriesService context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<Industry>> Get()
        {
            return await _context.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetIndustry")]
        public async Task<ActionResult<Industry>> Get([FromRoute] string id)
        {
            var industry = await _context.Get(id);

            if (industry == null)
            {
                return NotFound();
            }

            return Ok(industry);
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromBody] Industry industry)
        {
            await _context.Create(industry);
            return CreatedAtRoute("GetIndustry", new { id = industry.Id }, industry);
        }

        [HttpPut("{id:length(24)}")]
        public async Task<ActionResult> Update([FromRoute] string id, [FromBody] Industry industryIn)
        {
            if (industryIn.Id != id)
            {
                return BadRequest();
            }

            var industry = _context.Get(id);

            if (industry == null)
            {
                return NotFound();
            }

            await _context.Update(id, industryIn);

            return Ok(industryIn);
        }

        [HttpDelete("{id:length(24)}")]
        public async Task<ActionResult> Delete([FromRoute] string id)
        {
            var industry = await _context.Get(id);

            if (industry == null)
            {
                return NotFound();
            }

            await _context.Remove(industry.Id);
            return Ok();
        }
    }
}