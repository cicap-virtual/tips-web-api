﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using tips_web_api.Services;
using tips_web_api.Filters;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.HttpOverrides;

namespace tips_web_api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("AllowOrigin",
                    builder => builder
                        .AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod());
            });
            var config = new ServerConfig();
            var accesor = new HttpContextAccessor();
            Configuration.Bind(config);
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            var context = new TipsContext(config.MongoDB);

            services.AddSingleton<IUsersService>(new UsersService(context, accesor));
            services.AddSingleton<IRoleService>(new RoleService(context));
            services.AddSingleton<IProfilesService>(new ProfilesService(context));
            services.AddSingleton<ISurveysService>(new SurveyService(context));
            services.AddSingleton<ISurveyTypeService>(new SurveyTypesServices(context));
            services.AddSingleton<ISchedulesService>(new SchedulesService(context));
            services.AddSingleton<IAnswerSetService>(new AnswerSetService(context));
            services.AddSingleton<IQuestionsService>(new QuestionsService(context));
            services.AddSingleton<IMeasuresService>(new MeasuresService(context));
            services.AddSingleton<IMarketService>(new MarketService(context));
            services.AddSingleton<IPermissionService>(new PermissionService(context));
            services.AddSingleton<ICompletionsService>(new CompletionsService(context));
            services.AddSingleton<ICompaniesService>(new CompaniesServices(context));
            services.AddSingleton<IIndustriesService>(new IndustriesService(context));
            services.AddSingleton<IDivisionsService>(new DivisionsService(context));
            services.AddSingleton<IDepartmentsService>(new DepartmentsService(context));
            services.AddSingleton<IPositionsService>(new PositionsService(context));
            services.AddSingleton<IOfficeService>(new OfficesService(context));
            services.AddSingleton<IHierarchiesService>(new HierarchiesService(context));
            services.AddSingleton<ISurveyCategoriesService>(new SurveyCategoriesService(context));
            services.AddSingleton<IApiActionFilter>(provider => new ApiActionFilter(context));

            services.AddMvc(options =>
            {
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Tips API",
                    Version = "v1",
                    Description = "Tips API using MongoDB",
                });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseRouting();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Tips API V1");
            });

            app.UseCors("AllowOrigin");
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
