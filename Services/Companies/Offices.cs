using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{
    public class OfficesService : IOfficeService
    {
        private readonly ITipsContext _context;

        public OfficesService(ITipsContext context) => _context = context;

        public async Task<Office> Create(Office office)
        {
            await _context.Offices.InsertOneAsync(office);
            return office;
        }

        public async Task<IEnumerable<Office>> Get()
        {
            return await _context.Offices.Find(x => true).ToListAsync();
        }

        public async Task<Office> Get(string id)
        {
            return await _context.Offices.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task Remove(string id)
        {
            await _context.Offices.DeleteOneAsync(x => x.Id == id);
        }

        public async Task Update(string id, Office office)
        {
            await _context.Offices.ReplaceOneAsync(x => x.Id == id, office);
        }
    }

    public interface IOfficeService : IServiceContext<Office, Office>
    {

    }
}