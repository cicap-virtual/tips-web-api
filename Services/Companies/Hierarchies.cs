using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{
    public class HierarchiesService : IHierarchiesService
    {
        private readonly ITipsContext _context;

        public HierarchiesService(ITipsContext context)
        {
            _context = context;
        }

        public async Task<Hierarchy> Create(Hierarchy hierarchy)
        {
            await _context.Hierarchies.InsertOneAsync(hierarchy);
            return hierarchy;
        }

        public async Task<IEnumerable<Hierarchy>> Get()
        {
            return await _context.Hierarchies.Find(x => true).ToListAsync();
        }

        public async Task<Hierarchy> Get(string id)
        {
            return await _context.Hierarchies.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Hierarchy>> GetChildrens(string parentId)
        {
            return await _context.Hierarchies.Find(x => x.Parent.Id == parentId).ToListAsync();
        }

        public async Task<IEnumerable<Profile>> GetEmployees(string hierarchyId)
        {
            return await GetChildrensEmployees(hierarchyId);
        }

        private async Task<IEnumerable<Profile>> GetChildrensEmployees(string id)
        {
            List<Profile> employees = new List<Profile>();
            var hierarchy = await _context.Hierarchies.Find(x => x.Id == id).FirstOrDefaultAsync();
            if (hierarchy.IsPosition)
            {
                employees.AddRange(await GetByPosition(id));
                return employees;
            }

            var childrens = await _context.Hierarchies.Find(x => x.Parent.Id == id).ToListAsync();
            foreach (var level in childrens)
            {
                if (level.IsPosition)
                {
                    employees.AddRange(await GetByPosition(level.Id));
                }
                else
                {
                    employees.AddRange(await GetChildrensEmployees(level.Id));
                }
            }

            return employees;
        }

        private async Task<IEnumerable<Profile>> GetByPosition(string id)
        {
            return await _context.Profiles.Find(x => x.Position.Id == id).ToListAsync();
        }

        public async Task<IEnumerable<Hierarchy>> GetOrganization(string companyId)
        {
            return await _context.Hierarchies.Find(x => x.CompanyId == companyId).ToListAsync();
        }

        public async Task<IEnumerable<Hierarchy>> GetParents(string companyId)
        {
            return await _context.Hierarchies.Find(x => x.CompanyId == companyId && x.Parent.Id == null).ToListAsync();
        }

        public async Task Remove(string id)
        {
            await _context.Hierarchies.DeleteOneAsync(x => x.Id == id);
        }

        public async Task Update(string id, Hierarchy hierarchy)
        {
            await _context.Hierarchies.ReplaceOneAsync(x => x.Id == id, hierarchy);
        }

        public async Task<IEnumerable<Profile>> GetMultiplesEmployees(List<string> ids)
        {
            return await _context.Profiles.Find(x => ids.Contains(x.Position.Id)).ToListAsync();
        }
    }

    public interface IHierarchiesService : IServiceContext<Hierarchy, Hierarchy>
    {
        Task<IEnumerable<Hierarchy>> GetOrganization(string companyId);
        Task<IEnumerable<Hierarchy>> GetParents(string companyId);
        Task<IEnumerable<Hierarchy>> GetChildrens(string parentId);
        Task<IEnumerable<Profile>> GetEmployees(string hierarchyId);
        Task<IEnumerable<Profile>> GetMultiplesEmployees(List<string> ids);
    }
}