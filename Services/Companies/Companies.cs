using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using tips_web_api.Models;

namespace tips_web_api.Services
{
    public class CompaniesServices : ICompaniesService
    {
        private readonly ITipsContext _context;

        public CompaniesServices(ITipsContext context)
        {
            _context = context;
        }

        public async Task<Company> Create(Company company)
        {
            await _context.Companies.InsertOneAsync(company);
            return company;
        }

        public async Task<IEnumerable<CompanyInfo>> Get()
        {
            return await _context.Companies.Find(FilterDefinition<Company>.Empty)
                .Project(x => new CompanyInfo
                {
                    Id = x.Id,
                    Name = x.Name,
                    FullName = x.FullName,
                    EstablishedDate = x.EstablishedDate,
                    Industries = x.Industries,
                    CreateDate = x.CreateDate,
                    ModificateDate = x.ModificateDate
                }).ToListAsync();
        }

        public async Task<Company> Get(string id)
        {
            return await _context.Companies.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public Task<IEnumerable<Hierarchy>> GetDepartments(string companyId)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Hierarchy>> GetHierarchies(string companyId)
        {
            return await _context.Hierarchies.Find(x => x.CompanyId == companyId).ToListAsync();
        }

        public async Task<IEnumerable<OrgHierarchy>> GetOrgHierarchies(string companyId)
        {
            var parents = await _context.Hierarchies
                        .Find(x => x.CompanyId == companyId && x.Parent.Id == null)
                        .Project(x => new OrgHierarchy
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Code = x.Code,
                            IsPosition = x.IsPosition
                        }).SortByDescending(x => x.IsPosition)
                        .ToListAsync();

            foreach (var parent in parents)
            {
                parent.Children.AddRange(await GetChildrens(parent.Id));
            }
            return parents;
        }

        private async Task<IEnumerable<OrgHierarchy>> GetChildrens(string id)
        {
            var childrens = await _context.Hierarchies
                            .Find(x => x.Parent.Id == id)
                            .Project(x => new OrgHierarchy
                            {
                                Id = x.Id,
                                Name = x.Name,
                                Code = x.Code,
                                IsPosition = x.IsPosition
                            }).SortByDescending(x => x.IsPosition)
                            .ToListAsync();
            foreach (var item in childrens)
            {
                item.Children.AddRange(await GetChildrens(item.Id));
            }
            return childrens;
        }

        public async Task<IEnumerable<Hierarchy>> GetParentsHierarchy(string companyId)
        {
            return await _context.Hierarchies.Find(x => x.CompanyId == companyId && x.Parent.Id == null).ToListAsync();
        }

        public async Task<IEnumerable<Hierarchy>> GetPositions(string companyId)
        {
            return await _context.Hierarchies.Find(x => x.CompanyId == companyId && x.IsPosition).ToListAsync();
        }

        public async Task<IEnumerable<Profile>> GetProfiles(string companyId)
        {
            return await _context.Profiles.Find(x => x.Company.Id == companyId).ToListAsync();
        }

        public async Task Remove(string id)
        {
            await _context.Companies.DeleteOneAsync(x => x.Id == id);
        }

        public async Task Update(string id, Company company)
        {
            company.ModificateDate = DateTime.Now;
            await _context.Companies.ReplaceOneAsync(x => x.Id == id, company);
        }

        public async Task<IEnumerable<Schedule>> GetSchedules(string companyId)
        {
            return await _context.Schedules.Find(x => x.Company.Id == companyId).ToListAsync();
        }

        public async Task<IEnumerable<User>> GetAdministrators(string companyId)
        {
            return await _context.Users.Find(x => x.Companies.Any(y => y.Id == companyId)).ToListAsync();
        }

        public async Task<Profile> AddProfile(string id, ProfileItem item)
        {
            var company = await _context.Companies.Find(x => x.Id == id).FirstOrDefaultAsync();
            if (company == null)
            {
                throw new Exception();
            }

            var exists = await _context.Users.Find(x => x.Email == item.Email).FirstOrDefaultAsync();

            if (exists != null)
            {
                throw new Exception("Duplicate");
            }

            var role = await _context.Roles.Find(x => x.IsUser).FirstOrDefaultAsync();
            var user = new User()
            {
                UserName = item.Name,
                Email = item.Email
            };

            user.Roles.Add(role);
            await _context.Users.InsertOneAsync(user);
            var profile = new Profile()
            {
                Name = item.Name,
                Email = item.Email,
                Company = new Reference()
                {
                    Id = company.Id,
                    Name = company.Name
                }
            };

            var position = await _context.Hierarchies.Find(x => x.CompanyId == company.Id && x.Code == item.Code).FirstOrDefaultAsync();
            profile.User = new Reference() { Id = user.Id, Name = user.UserName };
            if (position != null)
            {
                profile.Position = position;
            }
            await _context.Profiles.InsertOneAsync(profile);
            return profile;
        }
    }

    public interface ICompaniesService : IServiceContext<Company, CompanyInfo>
    {
        Task<IEnumerable<Profile>> GetProfiles(string companyId);
        Task<Profile> AddProfile(string id, ProfileItem profile);
        Task<IEnumerable<User>> GetAdministrators(string companyId);
        Task<IEnumerable<Schedule>> GetSchedules(string companyId);
        Task<IEnumerable<Hierarchy>> GetHierarchies(string companyId);
        Task<IEnumerable<Hierarchy>> GetParentsHierarchy(string companyId);
        Task<IEnumerable<Hierarchy>> GetPositions(string companyId);
        Task<IEnumerable<Hierarchy>> GetDepartments(string companyId);
        Task<IEnumerable<OrgHierarchy>> GetOrgHierarchies(string companyId);
    }
}