using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{
    public class DepartmentsService : IDepartmentsService
    {
        private readonly ITipsContext _context;

        public DepartmentsService(ITipsContext context)
        {
            _context = context;
        }

        public async Task<Department> Create(Department department)
        {
            await _context.Departments.InsertOneAsync(department);
            return department;
        }

        public async Task<IEnumerable<Department>> Get()
        {
            return await _context.Departments.Find(x => true).ToListAsync();
        }

        public async Task<Department> Get(string id)
        {
            return await _context.Departments.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task Remove(string id)
        {
            await _context.Departments.DeleteOneAsync(x => x.Id == id);
        }

        public async Task Update(string id, Department department)
        {
            await _context.Departments.ReplaceOneAsync(x => x.Id == id, department);
        }
    }

    public interface IDepartmentsService : IServiceContext<Department, Department>
    {

    }
}