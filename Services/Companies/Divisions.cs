using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{
    public class DivisionsService : IDivisionsService
    {
        private readonly ITipsContext _context;

        public DivisionsService(ITipsContext context)
        {
            _context = context;
        }

        public async Task<Division> Create(Division division)
        {
            await _context.Divisions.InsertOneAsync(division);
            return division;
        }

        public async Task<IEnumerable<Division>> Get()
        {
            return await _context.Divisions.Find(x => true).ToListAsync();
        }

        public async Task<Division> Get(string id)
        {
            return await _context.Divisions.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task Remove(string id)
        {
            await _context.Divisions.DeleteOneAsync(x => x.Id == id);
        }

        public async Task Update(string id, Division division)
        {
            await _context.Divisions.ReplaceOneAsync(x => x.Id == id, division);
        }
    }

    public interface IDivisionsService : IServiceContext<Division, Division>
    {

    }
}