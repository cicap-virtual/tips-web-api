using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{
    public class PositionsService : IPositionsService
    {
        private readonly ITipsContext _context;

        public PositionsService(ITipsContext context)
        {
            _context = context;
        }

        public async Task<Position> Create(Position position)
        {
            await _context.Positions.InsertOneAsync(position);
            return position;
        }

        public async Task<IEnumerable<Position>> Get()
        {
            return await _context.Positions.Find(x => true).ToListAsync();
        }

        public async Task<Position> Get(string id)
        {
            return await _context.Positions.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task Remove(string id)
        {
            await _context.Positions.DeleteOneAsync(x => x.Id == id);
        }

        public async Task Update(string id, Position position)
        {
            await _context.Positions.ReplaceOneAsync(x => x.Id == id, position);
        }
    }

    public interface IPositionsService : IServiceContext<Position, Position>
    {

    }
}