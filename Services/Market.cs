using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{
    public class MarketService : IMarketService
    {
        private readonly ITipsContext _context;
        public MarketService(ITipsContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Survey>> GetSurveys()
        {
            return await _context.Surveys.Find(_ => _.Public && _.Final).Project<Survey>(Builders<Survey>.Projection.Exclude(x => x.Measures)).ToListAsync();
        }
    }

    public interface IMarketService
    {
        Task<IEnumerable<Survey>> GetSurveys();
    }
}