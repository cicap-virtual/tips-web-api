using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{
    public class QuestionsService : IQuestionsService
    {
        private readonly ITipsContext _context;
        public QuestionsService(ITipsContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Question>> Get()
        {
            return await _context.Questions.Find<Question>(question => true).ToListAsync();
        }

        public async Task<Question> Get(string id)
        {
            return await _context.Questions.Find<Question>(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<Question> Create(Question question)
        {
            await _context.Questions.InsertOneAsync(question);
            return question;
        }

        public async Task Update(string id, Question questionIn)
        {
            await _context.Questions.ReplaceOneAsync(question => question.Id == id, questionIn);
        }

        public async Task Remove(string id)
        {
            await _context.Questions.DeleteOneAsync(question => question.Id == id);
        }
    }

    public interface IQuestionsService : IServiceContext<Question, Question> { }
}