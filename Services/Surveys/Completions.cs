using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{
    public class CompletionsService : ICompletionsService
    {
        private readonly ITipsContext _context;

        public CompletionsService(ITipsContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Completion>> Get()
        {
            return await _context.Completions.Find(x => true).ToListAsync();
        }

        public async Task<Completion> Get(string completionId)
        {
            return await _context.Completions.Find(x => x.Id == completionId).FirstOrDefaultAsync();
        }

        public async Task<Completion> Create(Completion completion)
        {
            await _context.Completions.InsertOneAsync(completion);
            var filter = Builders<Schedule>.Filter
            .And(
                Builders<Schedule>.Filter.Eq(x => x.Id, completion.ScheduleId),
                Builders<Schedule>.Filter.ElemMatch(x => x.Participants, p => p.Id == completion.Profile.Id)
            );
            var update = Builders<Schedule>.Update
                .Set(x => x.Participants[-1].Status, "Completed")
                .Set(x => x.Participants[-1].CompletionId, completion.Id);
            await _context.Schedules.UpdateOneAsync(filter, update);
            return completion;
        }

        public async Task<Result> GetResult(string completionId)
        {
            var completion = await _context.Completions.Find(x => x.Id == completionId).FirstOrDefaultAsync();
            var survey = await _context.Surveys.Find(x => x.Id == completion.SurveyId).FirstOrDefaultAsync();
            var profile = await _context.Profiles.Find(x => x.Id == completion.Profile.Id).FirstOrDefaultAsync();
            var result = new Result()
            {
                Profile = profile,
                Survey = new Reference() { Id = survey.Id, Name = survey.Title },
                ScheduleId = completion.ScheduleId,
                CompletionId = completion.Id,
                CompleteDate = completion.CompleteDate
            };

            survey.Measures.ForEach(m =>
            {

                var measure = new ResultMeasure()
                {
                    MeasureId = m.Id,
                    Name = m.Name,
                    Weighting = Convert.ToDouble(m.Weighting)
                };
                if (survey.Type.HasMeasureQuestion)
                {
                    var current = completion.MeasureResponses.Where(x => x.Measure.Id == m.Id).FirstOrDefault();
                    measure.Value = current.Answer.Value;
                    m.Questions.ForEach(q =>
                    {
                        var question = new ResultQuestion()
                        {
                            QuestionId = q.Id,
                            Text = q.Text,
                            Value = current.Responses.Where(x => x.Question.Id == q.Id).FirstOrDefault().Answer.Value
                        };
                        measure.Questions.Add(question);
                    });
                    measure.SubMeasuresValue = measure.Questions.Average(x => x.Value);
                    result.Measures.Add(measure);
                }
                else
                {
                    m.Questions.ForEach(q =>
                                   {
                                       var question = new ResultQuestion()
                                       {
                                           QuestionId = q.Id,
                                           Text = q.Text,
                                           Weighting = Convert.ToDouble(q.Weighting),
                                           Value = completion.Responses.Where(x => x.Question.Id == q.Id).FirstOrDefault().Answer.Value
                                       };
                                       measure.Questions.Add(question);
                                   });
                    measure.Value = measure.Questions.Average(x => x.Value);
                    result.Measures.Add(measure);
                }

            });

            return result;
        }

        public async Task Remove(string completionId)
        {
            await _context.Completions.DeleteOneAsync(x => x.Id == completionId);
        }

        public async Task Update(string id, Completion completion)
        {
            await _context.Completions.ReplaceOneAsync(x => x.Id == id, completion);
        }
    }
    public interface ICompletionsService : IServiceContext<Completion, Completion>
    {
        Task<Result> GetResult(string id);
    }
}