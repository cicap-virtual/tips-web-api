using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{
    public class AnswerSetService : IAnswerSetService
    {
        private readonly ITipsContext _context;
        public AnswerSetService(ITipsContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<AnswerSet>> Get()
        {
            return await _context.AnswerSets.Find<AnswerSet>(x => true).ToListAsync();
        }

        public async Task<AnswerSet> Get(string id)
        {
            return await _context.AnswerSets.Find<AnswerSet>(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<AnswerSet> Create(AnswerSet set)
        {
            await _context.AnswerSets.InsertOneAsync(set);
            return set;
        }

        public async Task Update(string id, AnswerSet setIn)
        {
            await _context.AnswerSets.ReplaceOneAsync(set => set.Id == id, setIn);
        }

        public async Task Remove(string id)
        {
            await _context.AnswerSets.DeleteOneAsync(set => set.Id == id);
        }
    }

    public interface IAnswerSetService : IServiceContext<AnswerSet, AnswerSet>
    {

    }
}