using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{

    public class SurveyService : ISurveysService
    {
        private readonly ITipsContext _context;

        public SurveyService(ITipsContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Survey>> Get()
        {
            return await _context.Surveys.Find(_ => _.Final).Project<Survey>(Builders<Survey>.Projection.Exclude(x => x.Measures)).ToListAsync();
        }

        public async Task<Survey> Get(string id)
        {
            return await _context.Surveys.Find<Survey>(_ => _.Id == id).FirstOrDefaultAsync();
        }

        public async Task<long> GetCount()
        {
            return await _context.Surveys.CountDocumentsAsync(_ => _.Final);
        }

        public async Task<IEnumerable<Schedule>> GetSchedules(string id)
        {
            return await _context.Schedules.Find(x => x.Survey.Id == id).Project<Schedule>(Builders<Schedule>.Projection.Exclude(x => new { x.Survey, x.Participants })).ToListAsync();
        }

        public async Task<Survey> Create(Survey survey)
        {
            survey.CreatedDate = DateTime.Now;
            survey.ModificateDate = DateTime.Now;
            survey.Final = true;
            if (survey.Id != "")
            {
                await Update(survey.Id, survey);
                return survey;
            }
            survey.Measures.ForEach(m =>
            {
                if (m.MainQuestion != null)
                {
                    m.MainQuestion.Id = ObjectId.GenerateNewId().ToString();
                    _context.Questions.InsertOne(m.MainQuestion);
                }
                _context.Questions.InsertMany(m.Questions.Where(x => x.Id.Length == 0));
            });
            await _context.Measures.InsertManyAsync(survey.Measures.Where(x => x.Id.Length == 0));

            await _context.Surveys.InsertOneAsync(survey);

            return survey;
        }

        public async Task<Survey> SaveTemp(Survey survey)
        {
            survey.Final = false;
            if (survey.Id == "")
            {
                survey.CreatedDate = DateTime.Now;
                survey.Measures.ForEach(m =>
                {
                    m.Id = ObjectId.GenerateNewId().ToString();
                    m.Questions.ForEach(q =>
                    {
                        q.Id = ObjectId.GenerateNewId().ToString();
                    });
                });
                await _context.Surveys.InsertOneAsync(survey);
                return survey;
            }
            survey.CreatedDate = DateTime.Now;
            await _context.Surveys.ReplaceOneAsync(x => x.Id == survey.Id, survey);
            return survey;
        }

        public async Task<IEnumerable<Survey>> GetTemp(string id)
        {
            return await _context.Surveys.Find(_ => _.Final == false && _.CreatedBy.Id == id).ToListAsync();
        }

        public async Task Update(string id, Survey surveyIn)
        {
            surveyIn.ModificateDate = DateTime.Now;

            surveyIn.Measures.ForEach(async m =>
            {
                if (m.MainQuestion != null && string.IsNullOrEmpty(m.MainQuestion.Id))
                {
                    m.MainQuestion.Id = ObjectId.GenerateNewId().ToString();
                    _context.Questions.InsertOne(m.MainQuestion);
                }
                var questions = m.Questions.Where(x => string.IsNullOrEmpty(x.Id));
                if (questions.Count() > 0)
                {
                    _context.Questions.InsertMany(questions);
                }

                m.Questions.ForEach(async q =>
                {
                    if (q.Id == "")
                    {
                        await _context.Questions.InsertOneAsync(q);
                    }
                    else
                    {
                        await _context.Questions.ReplaceOneAsync(x => x.Id == q.Id, q);
                    }
                });
                if (m.Id == "")
                {
                    await _context.Measures.InsertOneAsync(m);
                }
                else
                {
                    await _context.Measures.ReplaceOneAsync(x => x.Id == m.Id, m);
                }
            });

            await _context.Surveys.ReplaceOneAsync(survey => survey.Id == id, surveyIn);
        }

        public async Task Remove(string id)
        {
            await _context.Surveys.DeleteOneAsync(survey => survey.Id == id);
        }

        public async Task<IEnumerable<Completion>> GetCompletions(string surveyId)
        {
            return await _context.Completions.Find(x => x.SurveyId == surveyId).ToListAsync();
        }
    }
    public interface ISurveysService : IServiceContext<Survey, Survey>
    {
        Task<long> GetCount();
        Task<Survey> SaveTemp(Survey survey);
        Task<IEnumerable<Survey>> GetTemp(string id);
        Task<IEnumerable<Schedule>> GetSchedules(string id);
        Task<IEnumerable<Completion>> GetCompletions(string surveyId);
    }
}