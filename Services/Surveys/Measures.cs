using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services {
    public class MeasuresService : IMeasuresService {
        private readonly ITipsContext _context;
        public MeasuresService(ITipsContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Measure>> Get()
        {
            return await _context.Measures.Find<Measure>(x => true).ToListAsync();
        }

        public async Task<Measure> Get(string id)
        {
            return await _context.Measures.Find<Measure>(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<Measure> Create(Measure measure)
        {
            await _context.Measures.InsertOneAsync(measure);
            return measure;
        }

        public async Task Update(string id, Measure measure)
        {
            measure.Questions.ForEach(async q =>
            {
                if (q.Id == "")
                {
                    await _context.Questions.InsertOneAsync(q);
                }
            });
            await _context.Measures.ReplaceOneAsync(x => x.Id == id, measure);
        }

        public async Task Remove(string id)
        {
            await _context.Measures.DeleteOneAsync(x => x.Id == id);
        }
    }
    public interface IMeasuresService : IServiceContext<Measure, Measure> {}
}