using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;
using tips_web_api.Shared;

namespace tips_web_api.Services
{
    public class SchedulesService : ISchedulesService
    {
        private readonly ITipsContext _context;

        public SchedulesService(ITipsContext context)
        {
            _context = context;
        }

        public async Task<Schedule> Create(Schedule schedule)
        {
            if (schedule.Company != null)
            {
                schedule.Company = await _context.Companies.Find(x => x.Id == schedule.Company.Id)
                    .Project(x => new Reference
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Title = x.FullName
                    })
                    .FirstOrDefaultAsync();
            }
            if (schedule.Hierarchies.Count() > 0)
            {
                schedule.Participants = await _context.Profiles
                    .Find(x => schedule.Hierarchies
                    .Contains(x.Position.Id))
                    .Project(x => new Participant
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Email = x.Email
                    }).ToListAsync();
            }
            await _context.Schedules.InsertOneAsync(schedule);
            return schedule;
        }

        public async Task<IEnumerable<Schedule>> Get()
        {
            return await _context.Schedules.Find(x => true).ToListAsync();
        }

        public async Task<Schedule> Get(string id)
        {
            return await _context.Schedules.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Completion>> GetCompletions(string scheduleId)
        {
            return await _context.Completions.Find(x => x.ScheduleId == scheduleId).ToListAsync();
        }

        public async Task<long> GetCount()
        {
            return await _context.Schedules.CountDocumentsAsync(x => true);
        }

        public async Task<IEnumerable<Result>> GetResults(string scheduleId)
        {
            var completions = await _context.Completions.Find(x => x.ScheduleId == scheduleId).ToListAsync();
            List<Result> results = new List<Result>();
            completions.ForEach(c =>
            {
                var result = GetResult(c);
                results.Add(result);
            });

            return results;
        }

        public async Task<Survey> GetSurvey(string id)
        {
            var schedule = await _context.Schedules.Find(x => x.Id == id).FirstOrDefaultAsync();
            var survey = await _context.Surveys.Find(x => x.Id == schedule.Survey.Id).FirstOrDefaultAsync();
            if (survey.Type.IsRandom)
            {
                survey.Measures.ForEach(m =>
                {
                    m.Questions.Shuffle();
                });
                survey.Measures.Shuffle();
            }
            return survey;
        }

        public async Task Remove(string id)
        {
            await _context.Schedules.DeleteOneAsync(schedule => schedule.Id == id);
        }

        private Result GetResult(Completion completion)
        {
            var survey = _context.Surveys.Find(x => x.Id == completion.SurveyId).FirstOrDefault();
            var profile = _context.Profiles.Find(x => x.Id == completion.Profile.Id).FirstOrDefault();
            var result = new Result()
            {
                Profile = profile,
                Survey = new Reference() { Id = survey.Id, Name = survey.Title },
                CompletionId = completion.Id,
            };

            survey.Measures.ForEach(m =>
            {
                var measure = new ResultMeasure()
                {
                    MeasureId = m.Id,
                    Name = m.Name
                };

                if (survey.Type.HasMeasureQuestion)
                {
                    var current = completion.MeasureResponses.Where(x => x.Measure.Id == m.Id).FirstOrDefault();
                    measure.Value = current.Answer.Value;
                    m.Questions.ForEach(q =>
                    {
                        var question = new ResultQuestion()
                        {
                            QuestionId = q.Id,
                            Text = q.Text,
                            Value = current.Responses.Where(x => x.Question.Id == q.Id).FirstOrDefault().Answer.Value
                        };
                        measure.Questions.Add(question);
                    });
                    measure.SubMeasuresValue = measure.Questions.Average(x => x.Value);
                }
                else
                {
                    m.Questions.ForEach(q =>
                                    {
                                        var question = new ResultQuestion()
                                        {
                                            QuestionId = q.Id,
                                            Text = q.Text,
                                            Value = completion.Responses.Where(x => x.Question.Id == q.Id).FirstOrDefault().Answer.Value
                                        };
                                        measure.Questions.Add(question);
                                    });
                    measure.Value = measure.Questions.Average(x => x.Value);
                }

                result.Measures.Add(measure);
            });

            return result;
        }

        public async Task Update(string id, Schedule schedule)
        {
            var update = Builders<Schedule>.Update
                .Set(x => x.Participants, schedule.Participants)
                .Set(x => x.Survey, schedule.Survey)
                .Set(x => x.Description, schedule.Description)
                .Set(x => x.DueDate, schedule.DueDate)
                .Set(x => x.StartDate, schedule.StartDate)
                .Set(x => x.ModificatedDate, DateTime.Now);
            await _context.Schedules.UpdateOneAsync(x => x.Id == id, update);
        }
    }

    public interface ISchedulesService : IServiceContext<Schedule, Schedule>
    {
        Task<Survey> GetSurvey(string id);
        Task<long> GetCount();
        Task<IEnumerable<Completion>> GetCompletions(string scheduleId);
        Task<IEnumerable<Result>> GetResults(string scheduleId);
    }
}