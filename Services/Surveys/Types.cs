using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{
    public class SurveyTypesServices : ISurveyTypeService
    {

        private readonly ITipsContext _context;

        public SurveyTypesServices(ITipsContext context)
        {
            _context = context;
        }

        public async Task<SurveyType> Create(SurveyType type)
        {
            await _context.SurveyTypes.InsertOneAsync(type);
            return type;
        }

        public async Task<IEnumerable<SurveyType>> Get()
        {
            return await _context.SurveyTypes.Find<SurveyType>(x => true).ToListAsync();
        }

        public async Task<SurveyType> Get(string id)
        {
            return await _context.SurveyTypes.Find<SurveyType>(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task Remove(string id)
        {
            await _context.SurveyTypes.DeleteOneAsync(type => type.Id == id);
        }

        public async Task Update(string id, SurveyType typeIn)
        {
            await _context.SurveyTypes.ReplaceOneAsync(type => type.Id == id, typeIn);
            var filter = Builders<Survey>.Filter.Where(x => x.Type.Id == typeIn.Id);
            var update = Builders<Survey>.Update.Set(x => x.Type, typeIn);
            await _context.Surveys.UpdateManyAsync(filter, update);
        }
    }

    public interface ISurveyTypeService : IServiceContext<SurveyType, SurveyType> { }
}