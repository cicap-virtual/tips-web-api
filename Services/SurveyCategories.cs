using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{
    public class SurveyCategoriesService : ISurveyCategoriesService
    {
        private readonly ITipsContext _context;
        public SurveyCategoriesService(ITipsContext context)
        {
            _context = context;
        }

        public async Task<SurveyCategory> Create(SurveyCategory element)
        {
            await _context.SurveyCategories.InsertOneAsync(element);
            return element;
        }

        public async Task<IEnumerable<Survey>> FilterSurveys(List<string> ids)
        {
            return await _context.Surveys.Find(x => ids.Any(y => y == x.Category.Id)).ToListAsync();
        }

        public async Task<IEnumerable<SurveyCategory>> Get()
        {
            return await _context.SurveyCategories.Find(x => true).ToListAsync();
        }

        public async Task<SurveyCategory> Get(string id)
        {
            var category = await _context.SurveyCategories.Find(x => x.Id == id).FirstOrDefaultAsync();
            return category;
        }

        public async Task<IEnumerable<Survey>> GetSurveys(string id)
        {
            return await _context.Surveys.Find(x => x.Category.Id == id).ToListAsync();
        }


        public async Task Remove(string id)
        {
            await _context.SurveyCategories.DeleteOneAsync(x => x.Id == id);
        }

        public Task Update(string id, SurveyCategory element)
        {
            throw new System.NotImplementedException();
        }
    }

    public interface ISurveyCategoriesService : IServiceContext<SurveyCategory, SurveyCategory>
    {
        Task<IEnumerable<Survey>> GetSurveys(string id);
        Task<IEnumerable<Survey>> FilterSurveys(List<string> ids);
    }
}