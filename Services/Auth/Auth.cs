using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{
	public class AuthService : IAuthService
	{
		private readonly ITipsContext _context;
		private IHttpContextAccessor _accessor;

		public AuthService(ITipsContext context, IHttpContextAccessor accessor)
		{
			_context = context;
			_accessor = accessor;
		}

		public async Task ChangePassword(string userId, string newPassword)
		{
			var user = _context.Users.Find(x => x.Id == userId).FirstOrDefaultAsync();
			if (user == null)
			{
				return;
			}
			var update = Builders<User>.Update.Set(x => x.Password, newPassword);
			await _context.Users.UpdateOneAsync(x => x.Id == userId, update);
		}

		public Task GetByEmail(string email)
		{
			throw new System.NotImplementedException();
		}

		public Task GetByUserName(string userName)
		{
			throw new System.NotImplementedException();
		}

		public async Task<UserInfo> Login(Login login)
		{
			var user = await _context.Users
				.Find(x => (x.Email.ToLower() == login.UserName.ToLower() || x.UserName.ToLower() == login.UserName.ToLower()) && x.Password == login.Password)
				.Project<UserInfo>(Builders<User>.Projection.Expression(x => new UserInfo()
				{
					Id = x.Id,
					UserName = x.UserName,
					Email = x.Email,
					Roles = x.Roles,
					Companies = x.Companies
				})).FirstOrDefaultAsync();

			return user;
		}

		public Task ResetPassword(string email)
		{
			throw new System.NotImplementedException();
		}

	}

	public interface IAuthService
	{
		Task<UserInfo> Login(Login login);
		Task ResetPassword(string email);
		Task GetByUserName(string userName);
		Task GetByEmail(string email);
		Task ChangePassword(string userId, string newPassword);

	}

	public class Login
	{
		public string UserName { get; set; }
		public string Password { get; set; }
	}

}