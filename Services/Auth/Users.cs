using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MongoDB.Driver;
using tips_web_api.Models;
using tips_web_api.Shared;

namespace tips_web_api.Services
{
    public class UsersService : IUsersService
    {
        private readonly ITipsContext _context;
        private IHttpContextAccessor _accesor;

        public UsersService(ITipsContext context, IHttpContextAccessor accesor)
        {
            _context = context;
            _accesor = accesor;
        }

        public async Task<IEnumerable<User>> Get()
        {
            return await _context.Users.Find(FilterDefinition<User>.Empty).ToListAsync();
        }

        public async Task<User> GetByEmail(string email)
        {
            return await _context.Users.Find(x => x.Email == email).FirstOrDefaultAsync();
        }

        public async Task<User> Get(string id)
        {
            return await _context.Users.Find<User>(user => user.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<User>> GetAdmins()
        {
            return await _context.Users.Find(x => x.Roles.Any(y => y.IsAdmin)).ToListAsync();
        }

        public async Task<IEnumerable<User>> GetCompanyAdmins()
        {
            return await _context.Users.Find(x => x.Roles.Any(y => y.IsCompanyAdmin)).ToListAsync();
        }
        public async Task<User> GetByUId(string uId)
        {
            return await _context.Users.Find(x => x.UId == uId).FirstOrDefaultAsync();
        }

        public async Task<Session> CreateSession(User user)
        {
            Session session = new Session()
            {
                SessionId = Util.GenerateID(10),
                User = new UserInfo() { Id = user.Id, UserName = user.UserName, Roles = user.Roles, Email = user.Email },
                LoginTime = DateTime.Now,
                IP = _accesor.HttpContext.Connection.RemoteIpAddress.ToString(),
                Browser = _accesor.HttpContext.Request.Headers["User-Agent"].ToString()
            };
            var profile = await _context.Profiles.Find(x => x.User.Id == user.Id).FirstOrDefaultAsync();
            if (profile != null)
            {
                session.Profile = profile;
            }
            await _context.Sessions.InsertOneAsync(session);
            return session;
        }

        public async Task DestroySession(string sessionId)
        {
            var update = Builders<Session>.Update.Set("LogoutTime", DateTime.Now);
            await _context.Sessions.UpdateOneAsync<Session>(x => x.SessionId == sessionId, update);
        }

        public async Task<User> Create(User user)
        {
            await _context.Users.InsertOneAsync(user);

            if (user.Roles.Any(x => x.IsUser))
            {
                Profile profile = new Profile()
                {
                    Name = user.UserName,
                    User = new Reference() { Name = user.UserName, Id = user.Id },
                    Email = user.Email,
                    RegisterDate = DateTime.Now
                };

                await _context.Profiles.InsertOneAsync(profile);
            }
            return user;
        }

        public async Task Update(string id, User userIn)
        {
            var update = Builders<User>.Update
                .Set(x => x.UserName, userIn.UserName)
                .Set(x => x.Email, userIn.Email)
                .Set(x => x.Roles, userIn.Roles)
                .Set(x => x.Approved, userIn.Approved)
                .Set(x => x.Companies, userIn.Companies)
                .Set(x => x.ModificateDate, DateTime.Now);

            if (userIn.Roles.Any(x => x.IsUser))
            {
                var profile = await _context.Profiles.Find(x => x.User.Id == id).FirstOrDefaultAsync();
                if (profile == null)
                {
                    Profile profileIn = new Profile()
                    {
                        Name = userIn.UserName,
                        User = new Reference() { Name = userIn.UserName, Id = userIn.Id },
                        Email = userIn.Email,
                        RegisterDate = DateTime.Now
                    };

                    await _context.Profiles.InsertOneAsync(profileIn);
                }
            }
            await _context.Users.UpdateOneAsync(x => x.Id == id, update);
        }

        public async Task Remove(string id)
        {
            await _context.Profiles.DeleteOneAsync(profile => profile.User.Id == id);
            await _context.Users.DeleteOneAsync(user => user.Id == id);
        }

        private bool Exists(string email)
        {
            return _context.Users.AsQueryable().Any(x => x.Email == email);
        }

        public async Task<User> FireBaseLogin(FireBaseLog log)
        {
            var user = await _context.Users.Find(x => x.UId == log.UId).FirstOrDefaultAsync();
            if (user == null)
            {
                user = await _context.Users.Find(x => x.Email == log.Email).FirstOrDefaultAsync();
                if (user == null)
                {
                    user = new User()
                    {
                        UId = log.UId,
                        Email = log.Email,
                        UserName = log.DisplayValue
                    };
                    await _context.Users.InsertOneAsync(user);
                    return user;
                }
                user.UId = log.UId;
                await _context.Users.ReplaceOneAsync<User>(x => x.Id == user.Id, user);
                return user;
            }

            return user;
        }

        public async Task<bool> EmailAvailable(string email)
        {
            var user = await _context.Users.Find(x => x.Email == email).FirstOrDefaultAsync();
            return (user == null);
        }
    }

    public interface IUsersService : IServiceContext<User, User>
    {
        Task<User> GetByEmail(string email);
        Task<User> GetByUId(string uId);
        Task<User> FireBaseLogin(FireBaseLog log);
        Task<IEnumerable<User>> GetAdmins();
        Task<IEnumerable<User>> GetCompanyAdmins();
        Task DestroySession(string sessionId);
        Task<Session> CreateSession(User user);
        Task<bool> EmailAvailable(string email);
    }

    public class FireBaseLog
    {
        public string UId { get; set; }
        public string Email { get; set; }
        public string DisplayValue { get; set; }
    }
}