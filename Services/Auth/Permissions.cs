using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{
    public class PermissionService : IPermissionService
    {
        private readonly ITipsContext _context;
        public PermissionService(ITipsContext context)
        {
            _context = context;
        }

        public async Task<Permission> Create(Permission permission)
        {
            await _context.Permissions.InsertOneAsync(permission);
            return permission;
        }

        public async Task Remove(string id)
        {
            await _context.Permissions.DeleteOneAsync(x => x.Id == id);
            var remove = Builders<Role>.Update
                .PullFilter(x => x.Permissions, y => y.Access.Id == id);
            await _context.Roles.UpdateManyAsync(FilterDefinition<Role>.Empty, remove);
        }

        public async Task<IEnumerable<Permission>> Get()
        {
            return await _context.Permissions.Find<Permission>(permission => true).ToListAsync();
        }

        public async Task<Permission> Get(string id)
        {
            return await _context.Permissions.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task Update(string id, Permission permissionIn)
        {
            var update = Builders<Permission>.Update
                .Set(x => x.Name, permissionIn.Name)
                .Set(x => x.Code, permissionIn.Code);
            await _context.Permissions.UpdateOneAsync(x => x.Id == permissionIn.Id, update);
            var filter = Builders<Role>.Filter.Where(x => x.Permissions.Any(y => y.Access.Id == permissionIn.Id));
            var updateRole = Builders<Role>.Update
                .Set(x => x.Permissions[-1].Access, permissionIn);

            await _context.Roles.UpdateManyAsync(filter, updateRole);
        }
    }

    public interface IPermissionService : IServiceContext<Permission, Permission> { }
}