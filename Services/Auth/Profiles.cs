using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{
    public class ProfilesService : IProfilesService
    {
        private readonly ITipsContext _context;

        public ProfilesService(ITipsContext context)
        {
            _context = context;
        }

        public async Task<Profile> Create(Profile profile)
        {
            var role = await _context.Roles.Find(x => x.IsUser).FirstOrDefaultAsync();
            var user = new User()
            {
                UserName = profile.Name,
                Email = profile.Email
            };

            user.Roles.Add(role);

            await _context.Users.InsertOneAsync(user);

            profile.User = new Reference() { Id = user.Id, Name = user.UserName };
            await _context.Profiles.InsertOneAsync(profile);
            return profile;
        }

        public async Task<IEnumerable<Profile>> Get()
        {
            return await _context.Profiles.Find(x => true).ToListAsync();
        }

        public async Task<Profile> Get(string id)
        {
            return await _context.Profiles.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<long> GetCount()
        {
            return await _context.Profiles.CountDocumentsAsync(x => true);
        }

        public async Task<IEnumerable<Schedule>> GetPendings(string id)
        {
            return await _context.Schedules
                .Find(x => x.Participants.Any(y => y.Id == id && y.Status == "Pending")
                    && x.StartDate <= DateTime.Today
                    && x.DueDate >= DateTime.Today)
                .Project<Schedule>(Builders<Schedule>.Projection.Exclude(x => x.Participants))
                .ToListAsync();
        }

        public async Task<IEnumerable<Profile>> GetProfiles(List<string> ids)
        {
            return await _context.Profiles.Find(x => ids.Contains(x.Id)).ToListAsync();
        }

        public async Task<Profile> GetUserProfile(string id)
        {
            return await _context.Profiles.Find(x => x.User.Id == id).FirstOrDefaultAsync();
        }

        public async Task Remove(string id)
        {
            await _context.Profiles.DeleteOneAsync(x => x.Id == id);
        }

        public async Task Update(string id, Profile profileIn)
        {
            var update = Builders<Profile>.Update
                .Set(x => x.Company, profileIn.Company)
                .Set(x => x.Position, profileIn.Position)
                .Set(x => x.Name, profileIn.Name)
                .Set(x => x.MobilePhone, profileIn.MobilePhone)
                .Set(x => x.OfficePhone, profileIn.OfficePhone)
                .Set(x => x.HomePhone, profileIn.HomePhone)
                .Set(x => x.Email, profileIn.Email)
                .Set(x => x.User, profileIn.User);

            await _context.Profiles.UpdateOneAsync(x => x.Id == id, update);
        }

        public async Task<Boolean> EmailAvailable(string email)
        {
            var profile = await _context.Users.Find(x => x.Email == email).FirstOrDefaultAsync();
            return (profile == null);
        }
    }

    public interface IProfilesService : IServiceContext<Profile, Profile>
    {
        Task<Profile> GetUserProfile(string id);
        Task<IEnumerable<Profile>> GetProfiles(List<string> ids);
        Task<long> GetCount();
        Task<IEnumerable<Schedule>> GetPendings(string id);
        Task<Boolean> EmailAvailable(string email);
    }
}