using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{
    public class RoleService : IRoleService
    {
        private readonly ITipsContext _context;
        public RoleService(ITipsContext context)
        {
            _context = context;
        }

        public async Task<Role> Create(Role role)
        {
            await _context.Roles.InsertOneAsync(role);
            return role;
        }

        public async Task Remove(string id)
        {
            await _context.Roles.DeleteOneAsync(x => x.Id == id);
            var update = Builders<User>.Update.PullFilter(x => x.Roles, y => y.Id == id);
            await _context.Users.UpdateManyAsync(FilterDefinition<User>.Empty, update);
        }

        public async Task<IEnumerable<Role>> Get()
        {
            return await _context.Roles.Find<Role>(role => true).ToListAsync();
        }

        public async Task<Role> Get(string id)
        {
            return await _context.Roles.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task Update(string id, Role roleIn)
        {
            await _context.Roles.ReplaceOneAsync(role => role.Id == id, roleIn);
            var filter = Builders<User>.Filter.Where(x => x.Roles.Any(y => y.Id == roleIn.Id));
            var update = Builders<User>.Update.Set(x => x.Roles[-1], roleIn);
            await _context.Users.UpdateManyAsync(filter, update);
        }
    }

    public interface IRoleService : IServiceContext<Role, Role> { }
}