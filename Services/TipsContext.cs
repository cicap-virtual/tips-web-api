using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{
    public class TipsContext : ITipsContext
    {
        private readonly IMongoDatabase db;

        public TipsContext(MongoDBConfig config)
        {
            var client = new MongoClient(config.ConnectionString);
            db = client.GetDatabase(config.Database);
        }

        public IMongoCollection<User> Users => db.GetCollection<User>("users");
        public IMongoCollection<Session> Sessions => db.GetCollection<Session>("sessions");
        public IMongoCollection<Measure> Measures => db.GetCollection<Measure>("measures");
        public IMongoCollection<Profile> Profiles => db.GetCollection<Profile>("profiles");
        public IMongoCollection<Role> Roles => db.GetCollection<Role>("roles");
        public IMongoCollection<Survey> Surveys => db.GetCollection<Survey>("surveys");
        public IMongoCollection<Schedule> Schedules => db.GetCollection<Schedule>("schedules");
        public IMongoCollection<Question> Questions => db.GetCollection<Question>("questions");
        public IMongoCollection<SurveyType> SurveyTypes => db.GetCollection<SurveyType>("survey_types");
        public IMongoCollection<AnswerSet> AnswerSets => db.GetCollection<AnswerSet>("answer_sets");
        public IMongoCollection<Permission> Permissions => db.GetCollection<Permission>("permissions");
        public IMongoCollection<Completion> Completions => db.GetCollection<Completion>("completions");
        public IMongoCollection<Company> Companies => db.GetCollection<Company>("companies");
        public IMongoCollection<Industry> Industries => db.GetCollection<Industry>("industries");
        public IMongoCollection<Division> Divisions => db.GetCollection<Division>("divisions");
        public IMongoCollection<Department> Departments => db.GetCollection<Department>("departments");
        public IMongoCollection<Position> Positions => db.GetCollection<Position>("positions");
        public IMongoCollection<Office> Offices => db.GetCollection<Office>("offices");
        public IMongoCollection<Hierarchy> Hierarchies => db.GetCollection<Hierarchy>("hierarchies");
        public IMongoCollection<SurveyCategory> SurveyCategories => db.GetCollection<SurveyCategory>("categories");
    }

    public interface ITipsContext
    {
        IMongoCollection<User> Users { get; }
        IMongoCollection<Session> Sessions { get; }
        IMongoCollection<Profile> Profiles { get; }
        IMongoCollection<Role> Roles { get; }
        IMongoCollection<Survey> Surveys { get; }
        IMongoCollection<Schedule> Schedules { get; }
        IMongoCollection<Question> Questions { get; }
        IMongoCollection<SurveyType> SurveyTypes { get; }
        IMongoCollection<AnswerSet> AnswerSets { get; }
        IMongoCollection<Measure> Measures { get; }
        IMongoCollection<Permission> Permissions { get; }
        IMongoCollection<Completion> Completions { get; }
        IMongoCollection<Company> Companies { get; }
        IMongoCollection<Industry> Industries { get; }
        IMongoCollection<Division> Divisions { get; }
        IMongoCollection<Department> Departments { get; }
        IMongoCollection<Position> Positions { get; }
        IMongoCollection<Office> Offices { get; } 
        IMongoCollection<Hierarchy> Hierarchies { get; }
        IMongoCollection<SurveyCategory> SurveyCategories { get; }
    }

    public interface IServiceContext<T, D>
    {
        Task<IEnumerable<D>> Get();
        Task<T> Get(string id);
        Task<T> Create(T element);
        Task Update(string id, T element);
        Task Remove(string id);
    }
}