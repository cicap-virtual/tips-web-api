using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using tips_web_api.Models;

namespace tips_web_api.Services
{
    public class IndustriesService : IIndustriesService
    {
        private readonly ITipsContext _context;

        public IndustriesService(ITipsContext context)
        {
            _context = context;
        }

        public async Task<Industry> Create(Industry industry)
        {
            await _context.Industries.InsertOneAsync(industry);
            return industry;
        }

        public async Task<IEnumerable<Industry>> Get()
        {
            return await _context.Industries.Find(x => true).ToListAsync();
        }

        public async Task<Industry> Get(string id)
        {
            return await _context.Industries.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task Remove(string id)
        {
            await _context.Industries.DeleteOneAsync(x => x.Id == id);
        }

        public async Task Update(string id, Industry industry)
        {
            await _context.Industries.ReplaceOneAsync(x => x.Id == id, industry);
        }
    }

    public interface IIndustriesService : IServiceContext<Industry, Industry> { }
}