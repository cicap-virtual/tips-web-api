using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace tips_web_api.Models
{
    public partial class Measure
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        
        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("weighting")]
        public decimal Weighting { get; set; }

        [BsonElement("subMeasures")]
        public List<Measure> SubMeasures { get; set; }

        [BsonElement("question")]
        public Question MainQuestion { get; set; }

        [BsonElement("questions")]
        public List<Question> Questions { get; set; }

    }
}