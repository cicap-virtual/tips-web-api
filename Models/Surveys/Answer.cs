using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace tips_web_api.Models
{
    public partial class Answer
    {
        [BsonElement("text")]
        public string Text { get; set; }

        [BsonElement("sort")]
        [BsonRepresentation(BsonType.Int32)]
        public int Sort { get; set; }

        [BsonElement("value")]
        [BsonRepresentation(BsonType.Int32)]
        public int Value { get; set; }

        [BsonElement("reverseValue")]
        [BsonRepresentation(BsonType.Int32)]
        public int ReverseValue { get; set; }

    }
}