using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace tips_web_api.Models
{
    public partial class Completion
    {
        public Completion()
        {
            CompleteDate = DateTime.Now;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("profile")]
        public Reference Profile { get; set; }

        [BsonElement("scheduleId")]
        public string ScheduleId { get; set; }

        [BsonElement("surveyId")]
        public string SurveyId { get; set; }

        [BsonElement("responses")]
        public List<Response> Responses { get; set; }

        [BsonElement("measureResponses")]
        public List<MeasureResponse> MeasureResponses { get; set; }

        [BsonElement("completeDate")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime CompleteDate { get; set; }
    }
}