using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace tips_web_api.Models
{
    public partial class Survey
    {
        public Survey()
        {
            Measures = new List<Measure>();
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("type")]
        public SurveyType Type { get; set; }

        [BsonElement("Description")]
        public string Description { get; set; }

        [BsonElement("createdDate")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime? CreatedDate { get; set; }

        [BsonElement("modificateDate")]
        public DateTime? ModificateDate { get; set; }

        [BsonElement("creaditsCost")]
        public double CreditsCost { get; set; }

        [BsonElement("category")]
        public SurveyCategory Category { get; set; }

        [BsonElement("measures")]
        public List<Measure> Measures { get; set; }

        [BsonElement("startDate")]
        public DateTime StartDate { get; set; }

        [BsonElement("createdBy")]
        public UserInfo CreatedBy { get; set; }

        [BsonElement("active")]
        public bool Active { get; set; }

        [BsonElement("public")]
        public bool Public { get; set; }

        [BsonElement("final")]
        public bool Final { get; set; }

        public Range Range { get; set; }

    }

    public class Range
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("text")]
        public string Text { get; set; }

        [BsonElement("minimum")]
        public decimal Minimum { get; set; }

        [BsonElement("maximum")]
        public decimal Maximum { get; set; }
    }
}