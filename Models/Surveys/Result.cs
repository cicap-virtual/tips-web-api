using System;
using System.Collections.Generic;

namespace tips_web_api.Models
{
    public class Result
    {
        public Result()
        {
            Measures = new List<ResultMeasure>();
        }
        public Reference Survey { get; set; }
        public string ScheduleId { get; set; }
        public Profile Profile { get; set; }
        public string CompletionId { get; set; }
        public DateTime CompleteDate { get; set; }
        public List<ResultMeasure> Measures { get; set; }

    }

    public class ResultMeasure
    {
        public ResultMeasure()
        {
            Questions = new List<ResultQuestion>();
        }

        public string MeasureId { get; set; }
        public string Name { get; set; }
        public double Value { get; set; }
        public double SubMeasuresValue { get; set; }
        public double Weighting { get; set; }
        public ResultQuestion MainQuestion { get; set; }
        public List<ResultQuestion> Questions { get; set; }
    }

    public class ResultQuestion
    {
        public string QuestionId { get; set; }
        public string Text { get; set; }
        public double Weighting { get; set; }
        public int Value { get; set; }
    }
}