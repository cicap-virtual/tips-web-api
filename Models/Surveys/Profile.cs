using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace tips_web_api.Models
{
    [BsonIgnoreExtraElements]
    public partial class Profile
    {
        public Profile()
        {
            RegisterDate = DateTime.Now;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("mobilePhone")]
        public string MobilePhone { get; set; }

        [BsonElement("officePhone")]
        public string OfficePhone { get; set; }

        [BsonElement("homePhone")]
        public string HomePhone { get; set; }

        [BsonElement("registerDate")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime RegisterDate { get; set; }

        [BsonElement("company")]
        public Reference Company { get; set; }

        [BsonElement("position")]
        public Hierarchy Position { get; set; }

        [BsonElement("user")]
        public Reference User { get; set; }

    }

    public partial class ProfileItem
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Code { get; set; }
    }
}