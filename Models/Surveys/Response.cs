using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace tips_web_api.Models
{
    public partial class Response
    {
        [BsonElement("question")]
        public Question Question { get; set; }

        [BsonElement("answer")]
        public Answer Answer { get; set; }
    }


    public partial class MeasureResponse
    {

        [BsonElement("measure")]
        public Reference Measure { get; set; }

        [BsonElement("mainQuestion")]
        public Question MainQuestion { get; set; }

        [BsonElement("answer")]
        public Answer Answer { get; set; }

        [BsonElement("responses")]
        public List<Response> Responses { get; set; }
    }
}