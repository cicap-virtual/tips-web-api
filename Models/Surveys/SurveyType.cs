using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace tips_web_api.Models
{
    public partial class SurveyType
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("hasRadar")]
        public bool HasRadar { get; set; }

        [BsonElement("hasBar")]
        public bool HasBar { get; set; }

        [BsonElement("hasMeasureQuestion")]
        public bool HasMeasureQuestion { get; set; }

        [BsonElement("prefix")]
        public string Prefix { get; set; }

        [BsonElement("measureName")]
        public string MeasureName { get; set; }

        [BsonElement("subMeasureName")]
        public string SubMeasureName { get; set; }

        [BsonElement("visibleMeaures")]
        public bool VisibleMeasures { get; set; }

        [BsonElement("wheelCharts")]
        public bool WheelCharts { get; set; }

        [BsonElement("instructions")]
        public string Instructions { get; set; }

        [BsonElement("isRandom")]
        public bool IsRandom { get; set; }

    }
}