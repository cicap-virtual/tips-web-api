using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace tips_web_api.Models
{
    [BsonIgnoreExtraElements]
    public partial class Schedule
    {
        public Schedule()
        {
            CreatedDate = DateTime.Now;
            Participants = new List<Participant>();
            Hierarchies = new List<string>();
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("assignedBy")]
        public Reference AssignedBy { get; set; }

        [BsonElement("startDate")]
        public DateTime StartDate { get; set; }

        [BsonElement("dueDate")]
        public DateTime DueDate { get; set; }

        [BsonElement("survey")]
        [BsonRequired]
        public Reference Survey { get; set; }

        [BsonElement("participants")]
        public List<Participant> Participants { get; set; }

        [BsonElement("createdDate")]
        public DateTime CreatedDate { get; set; }

        [BsonElement("modificatedDate")]
        public DateTime ModificatedDate { get; set; }

        [BsonRepresentation(BsonType.Int32)]
        public int Completed { get { return Participants.Where(x => x.Status == "Completed").ToList().Count; } }

        public int ParticipantsCount { get { return Participants.Count; } }

        public string Status
        {
            get
            {
                if (StartDate > DateTime.Today)
                {
                    return "Scheduled";
                }
                else
                {
                    if (DueDate >= DateTime.Today)
                    {
                        return "Active";
                    }
                    else
                    {
                        return "Expired";
                    }
                };
            }
        }

        [BsonElement("company")]
        public Reference Company { get; set; }

        [BsonElement("hierarchies")]
        public List<string> Hierarchies { get; set; }
    }

    public class Participant
    {
        public Participant()
        {
            Status = "Pending";
        }
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonElement("id")]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("status")]
        public string Status { get; set; }

        [BsonElement("completionId")]
        public string CompletionId { get; set; }
    }


    public class Filter
    {
        [BsonElement("companyId")]
        public string CompanyId { get; set; }

        [BsonElement("divisionId")]
        public string DivisionId { get; set; }

        [BsonElement("departmentId")]
        public string DepartmentId { get; set; }

        [BsonElement("positionId")]
        public string PositionId { get; set; }
    }

}