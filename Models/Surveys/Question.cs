using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace tips_web_api.Models
{
    public partial class Question
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id {get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("text")]
        public string Text { get; set; }

        [BsonElement("weighting")]
        public decimal Weighting { get; set; }

        [BsonElement("reverse")]
        [BsonRepresentation(BsonType.Boolean)]
        public bool Reverse { get; set; }

        [BsonElement("multiAnswer")]
        [BsonRepresentation(BsonType.Boolean)]
        public bool MultiAnswer { get; set; }

        [BsonElement("answerSet")]
        public AnswerSet AnswerSet { get; set; }

    }
}