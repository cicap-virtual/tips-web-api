using System;
using MongoDB.Bson.Serialization.Attributes;

namespace tips_web_api.Models
{
    public class Pending
    {
        [BsonElement("schedule")]
        public Reference Schedule { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("startDate")]
        public DateTime StartDate { get; set; }

        [BsonElement("dueDate")]
        public DateTime DueDate { get; set; }
    }
}