using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace tips_web_api.Models
{
    [BsonIgnoreExtraElements]
    public partial class Reference
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }
    }
}