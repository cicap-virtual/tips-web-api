using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace tips_web_api.Models
{
    [BsonIgnoreExtraElements]
    public class Hierarchy
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("companyId")]
        [BsonRepresentation(BsonType.ObjectId)]
        public string CompanyId { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("code")]
        public string Code { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("isPosition")]
        public bool IsPosition { get; set; }

        [BsonElement("parent")]
        public Reference Parent { get; set; }
    }

    public class OrgHierarchy
    {
        public OrgHierarchy() {
            Children = new List<OrgHierarchy>();
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public bool IsPosition { get; set; }
        public List<OrgHierarchy> Children { get; set; }
    }
}