using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace tips_web_api.Models
{
    public class Company
    {
        public Company()
        {
            Industries = new List<Industry>();
            CreateDate = DateTime.Now;
            ModificateDate = DateTime.Now;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("fullName")]
        public string FullName { get; set; }

        [BsonElement("establishedDate")]
        public DateTime EstablishedDate { get; set; }

        [BsonElement("address")]
        public string Address { get; set; }

        [BsonElement("industries")]
        public List<Industry> Industries { get; set; }

        [BsonElement("createDate")]
        public DateTime CreateDate { get; set; }

        [BsonElement("modificateDate")]
        public DateTime ModificateDate { get; set; }
    }

    [BsonIgnoreExtraElements]
    public class Division
    {
        public Division()
        { }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }
    }

    [BsonIgnoreExtraElements]
    public class Department
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("division")]
        public Reference Division { get; set; }
    }

    [BsonIgnoreExtraElements]
    public class Office
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("department")]
        public Reference Department { get; set; }
    }

    public class Position
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("level")]
        public int Level { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("division")]
        public Reference Division { get; set; }

        [BsonElement("department")]
        public Reference Department { get; set; }

        [BsonElement("office")]
        public Reference Office { get; set; }

        [BsonElement("active")]
        public bool Active { get; set; }
    }


    public class CompanyInfo
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public DateTime EstablishedDate { get; set; }
        public List<Industry> Industries { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModificateDate { get; set; }

    }

}

