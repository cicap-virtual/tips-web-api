using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace tips_web_api.Models
{
    [BsonIgnoreExtraElements]
    public partial class User
    {
        public User()
        {
            Roles = new List<Role>();
            CreatedDate = DateTime.Now;
            ModificateDate = DateTime.Now;
            Approved = false;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("uid")]
        public string UId { get; set; }

        [BsonElement("userName")]
        public string UserName { get; set; }

        [BsonRequired]
        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("approved")]
        public bool Approved { get; set; }

        [BsonElement("password")]
        public string Password { get; set; }

        [BsonElement("companies")]
        public List<Reference> Companies { get; set; }

        public bool Logged
        {
            get
            {
                return !String.IsNullOrEmpty(UId);
            }
        }

        [BsonElement("creditsBalance")]
        public double CreditsBalance { get; set; }

        [BsonElement("roles")]
        public List<Role> Roles { get; set; }

        [BsonElement("createdDate")]
        public DateTime CreatedDate { get; set; }

        [BsonElement("modificateDate")]
        public DateTime ModificateDate { get; set; }
    }

    public partial class UserInfo
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("userName")]
        public string UserName { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("roles")]
        public List<Role> Roles { get; set; }
        public List<Reference> Companies { get; set; }
    }
}