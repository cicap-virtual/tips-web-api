using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace tips_web_api.Models
{
    public class Role
    {
        public Role()
        {
            Permissions = new List<RolePermissions>();
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("isUser")]
        public bool IsUser { get; set; }

        [BsonElement("isCompanyAdmin")]
        public bool IsCompanyAdmin { get; set; }

        [BsonElement("isAdmin")]
        public bool IsAdmin { get; set; }

        [BsonElement("isCreator")]
        public bool IsCreator { get; set; }

        [BsonElement("isExternal")]
        public bool IsExternal { get; set; }

        [BsonElement("permissions")]
        public List<RolePermissions> Permissions { get; set; }
    }

    public class RolePermissions
    {
        [BsonElement("access")]
        public Permission Access { get; set; }

        [BsonElement("read")]
        public bool Read { get; set; }

        [BsonElement("edit")]
        public bool Edit { get; set; }

        [BsonElement("create")]
        public bool Create { get; set; }

        [BsonElement("delete")]
        public bool Delete { get; set; }

    }
}