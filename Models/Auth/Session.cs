using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace tips_web_api.Models
{
    public partial class Session
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("sessionId")]
        public string SessionId { get; set; }

        [BsonElement("loginTime")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime? LoginTime { get; set; }

        [BsonElement("logoutTime")]
        [BsonRepresentation(BsonType.DateTime)]
        public DateTime? LogoutTime { get; set; }

        [BsonElement("IP")]
        public string IP { get; set; }

        [BsonElement("user")]
        public UserInfo User { get; set; }

        [BsonElement("profile")]
        public Profile Profile { get; set; }

        [BsonElement("browser")]
        public string Browser { get; set; }
    }
}
