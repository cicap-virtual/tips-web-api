using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace tips_web_api.Models
{
    public class SurveyCategory
    {
        public SurveyCategory()
        {
            Children = new List<SurveyCategory>();
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("text")]
        public string Text { get; set; }

        [BsonElement("value")]
        public int Value { get; set; }

        [BsonElement("children")]
        public List<SurveyCategory> Children { get; set; }
    }
}