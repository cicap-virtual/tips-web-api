using System;
using System.IO;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace RegistrationWeb.Middleware
{
    public class ResponseWrapper
    {
        private readonly RequestDelegate _next;
        public ResponseWrapper(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var currentBody = context.Response.Body;

            using (var memoryStream = new MemoryStream())
            {
                context.Response.Body = memoryStream;

                await _next(context);
                //reset the body 
                context.Response.Body = currentBody;
                context.Response.ContentLength = null;

                memoryStream.Seek(0, SeekOrigin.Begin);
                object objResult = null;
                var readToEnd = new StreamReader(memoryStream).ReadToEnd();
                try
                {
                    objResult = JsonSerializer.Deserialize<object>(readToEnd);
                }
                catch (System.Exception)
                {
                }
                object result;
                if (context.Response.StatusCode == (int)HttpStatusCode.OK
                || context.Response.StatusCode == (int)HttpStatusCode.Created
                || context.Response.StatusCode == (int)HttpStatusCode.Accepted)
                {
                    result = CommonApiResponse.Create((HttpStatusCode)context.Response.StatusCode, objResult, "Successfully executed!");
                }
                else
                {
                    result = ErrorApiResponse.Create((HttpStatusCode)context.Response.StatusCode, readToEnd);
                }
                if (context.Response.StatusCode != (int)HttpStatusCode.NoContent)
                {
                    await context.Response.WriteAsync(JsonSerializer.Serialize(result));
                }
            }
        }
    }

    public static class ResponseWrapperExtensions
    {
        public static IApplicationBuilder UseResponseWrapper(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ResponseWrapper>();
        }
    }

    public class CommonApiResponse
    {
        public static CommonApiResponse Create(HttpStatusCode statusCode, object result = null, string errorMessage = null)
        {
            return new CommonApiResponse(statusCode, result, errorMessage);
        }

        public string version => "0.1";

        public int statusCode { get; set; }
        public string requestId { get; }
        public string message { get; set; }
        public object data { get; set; }

        protected CommonApiResponse(HttpStatusCode _statusCode, object _result = null, string _message = null)
        {
            requestId = Guid.NewGuid().ToString();
            statusCode = (int)_statusCode;
            data = _result;
            message = _message;
        }
    }

    public class ErrorApiResponse
    {
        public static ErrorApiResponse Create(HttpStatusCode statusCode, string message = null)
        {
            return new ErrorApiResponse(statusCode, message);
        }

        public string version => "0.1";

        public int statusCode { get; set; }
        public string requestId { get; }
        public string message { get; set; }
        public object data { get; set; }

        protected ErrorApiResponse(HttpStatusCode _statusCode, string _errorMessage = null)
        {
            requestId = Guid.NewGuid().ToString();
            statusCode = (int)_statusCode;
            message = _errorMessage;
            data = null;
        }
    }
}
