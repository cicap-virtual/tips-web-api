using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using MongoDB.Driver;
using tips_web_api.Models;
using tips_web_api.Services;

namespace tips_web_api.Filters
{
    public class SkipActionFilter : Attribute
    {
        public SkipActionFilter() { }
    }

    public interface IApiActionFilter
    {
        void OnActionExecuting(ActionExecutingContext context);
    }

    public class ApiActionFilter : ActionFilterAttribute, IApiActionFilter
    {
        private readonly ITipsContext _context;

        public ApiActionFilter(ITipsContext context)
        {
            _context = context;
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var attrs = (context.ActionDescriptor as ControllerActionDescriptor).MethodInfo.GetCustomAttributes(inherit: true);
            foreach (var attr in attrs)
            {
                if (attr.GetType() == typeof(SkipActionFilter))
                {
                    return;
                }
            }

            var request = context.HttpContext.Request;
            string userId = request.Headers["userId"];
            string sessionId = request.Headers["sessionId"];

            if (userId == null || userId.Length == 0)
            {
                context.Result = new ContentResult
                {
                    Content = "UserId not provided",
                    StatusCode = 401
                };
                return;
            }

            if (sessionId == null)
            {
                context.Result = new ContentResult
                {
                    Content = "SessionId not provided",
                    StatusCode = 401
                };
                return;
            }

            Session session = _context.Sessions.Find(x => x.User.Id == userId && x.SessionId == sessionId).FirstOrDefault();

            if (session == null)
            {
                context.Result = new ContentResult
                {
                    Content = "Session doesn't exists",
                    StatusCode = 401
                };
                return;
            }
            context.RouteData.Values.Add("userId", userId);
            context.RouteData.Values.Add("session", session);
        }
    }
}